import random
import lib
import time


n = 100000  # Промежуток
stop = 1000000
step = 100000
search = 0

try:
    search = int(input('Enter the search radius = '))
except ValueError:
    print('Error')
    exit()

file = open('time.txt', 'w')  # Работаем с файлом time.txt, если его нет, создаем

while n <= stop:  # Пока
    x = [random.uniform(-50, 50) for point in range(n)]  # 10*5 создаем случайных чисел по оси x
    y = [random.uniform(-50, 50) for point in range(n)]  # По оси y
    z = (random.uniform(-50, 50), random.uniform(-50, 50))  # По оси z

    starttime = time.time()  # Время начала алгоритма
    print(f'Number of points: {lib.counter_points(search, x, y, z)}')
    stoptime = time.time()  # Время конца алгоритма
    amounttime = str(stoptime - starttime).replace('.', ',')  # Количество времени потраченного на алгоритм
    file.write(f'{amounttime}\n')  # Запись количества времени потраченного на алгоритм в файл

    n += step

file.close()
