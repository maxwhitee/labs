import math
# Блок ввода данных программы и ее проверки
try:
    x = float(input("Введите x: "))
    a = float(input("Введите a: "))
    n = int(input('Выберите функцию 1,2,3: '))
except ValueError:
    print("Введенное значение - не число.")
    exit(1)
# Блок выбора функций и расчета G, F, Y
if n != 1 and n != 2 and n != 3:
    print("Вы ввели неверный номер функции. Попробуйте еще раз.")
if n == 1:
    try:
        G = -(8*(7*a**2+34*a*x-5*x**2)/(27*a**2+33*a*x+10*x**2))
        print('G = {0:.3f}' .format(G))
    except ZeroDivisionError:
        print("Ошибка!На ноль делить нельзя.")
elif n == 2:
    try:
        F = -(1/(math.sin(72*a**2-5*a*x-12*x**2-(math.pi/2))))
        print('F = {0:.3f}' .format(F))
    except ValueError:
        print("Ошибка!")
elif n == 3:
    try:
        Y = (math.log(42*a**2+53*a*x+15*x**2+1))
        print('Y = {0:.3f}' .format(Y))
    except ValueError:
        print("Ошибка!")