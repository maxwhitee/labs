import math


rnd = True
result_dict = {'G': [], 'F': [], 'Y': []}

while rnd:

    try:
        a = float(input('Введите а:'))
        x_min = float(input('Введите минимальное значение x:'))
        x_max = float(input('Введите максимальное значение x:'))
        n = int(input('Введите количество шагов вычисления функции:'))
    except ValueError:
        print('Ошибка ввода')
        exit(1)

    x_lst = []  # Список x

    step = (x_max - x_min) / n

    for i in range(n):
        x = x_min + step * i
        x_lst.append(x)
        g1 = -(8*(7*a**2+34*a*x-5*x**2)) # числитель функции G
        g2 = (27*a**2+33*a*x+10*x**2)  # знаменатель функции G
        if math.isclose(g2, 0, abs_tol=0.0001):
            G = None
            result_dict['G'].append(G)
        else:
            G = g1 / g2
            result_dict['G'].append(G)

    for i in range(n):
        x = x_min + step * i
        d = math.sin(72*a**2-5*a*x-12*x**2-(math.pi/2))  # знаменатель функции F
        if math.isclose(d, 0, abs_tol=0.0001):
            F = None
            result_dict['F'].append(F)
        else:
            F = (-1) / d
            result_dict['F'].append(F)

    for i in range(n):
        x = x_min + step * i
        m = math.log(42*a**2+53*a*x+15*x**2+1)
        if -1 <= m <= 1:
            Y = math.asin(m)
            result_dict['Y'].append(Y)
        else:
            Y = None
            result_dict['Y'].append(Y)

    # Вывод значений аргумента и функций

    print(f'x: {", ".join([str(x) for x in x_lst])}')
    print(f'G: {", ".join(["""Нет решения""" if y is None else str(f"""{y:.3f}""") for y in result_dict["G"]])}')
    print(f'F: {", ".join(["""Нет решения""" if y is None else str(f"""{y:.3f}""") for y in result_dict["F"]])}')
    print(f'Y: {", ".join(["""Нет решения""" if y is None else str(f"""{y:.5f}""") for y in result_dict["Y"]])}')

    # Продолжение или завершение цикла

    print('Если хотите продолжить нажмите 1, иначе - нажмите любую другую цифру')

    if int(input()) != 1:
        rnd = False
    else:
        x_lst = []
        result_dict["G"] = []
        result_dict["F"] = []
        result_dict["Y"] = []

