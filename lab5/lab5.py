import math

def main():
    mw = []
    try:
        command = int(input("G (1) or F (2) or Y (3)?: "))
        data = list(map(float, input('Enter а,x,х maximum, number of calculation steps ').split()))
        search = str(input('What match should look for? '))
        a = int(data[0])
        x = int(data[1])
        xmax = int(data[2])
        number = int(data[3])
        size = (xmax-x)/number  # Вычисляем размерность шагов
    except ValueError:
        print("The input is incorrect")
        return 1


    step = 0  # Шаг

    for i in range(number):
        if command == 1:
            try:
                g = -(8*(7*a**2+34*a*x-5*x**2)/(27*a**2+33*a*x+10*x**2))
                x += size
                step += 1
                print(step,"x=",x, "G = {0:.1f} ".format(g))
                mw.append(g)

            except ZeroDivisionError:
                g = None
                x += size
                step += 1
                print(step, "x=", x, "G = {0:.1f} ".format(g))

        elif command == 2:
            try:
                f = -(1/(math.sin(72*a**2-5*a*x-12*x**2-(math.pi/2))))
                x += size
                step += 1
                print(step,"x=",x, "F = {0:.1f} " .format(f))
                mw.append(f)

            except ValueError:
                f = None
                x += size
                step += 1
                print(step, "x=", x, "F = {0:.1f} " .format(f))

        elif command == 3:
            try:
                
                y = (math.log(42*a**2+53*a*x+15*x**2+1))
                x += size
                step += 1
                print(step,"x=",x, "Y = {0:.1f} " .format(y))
                mw.append(y)

            except ValueError:
                y = None
                x += size
                step += 1
                print(step, "x=", x, "Y = {0:.1f} " .format(y))
        else:
            print("There is no such command")

    mw = list(map(str, mw))
    print("Result- {0}".format(', '.join(mw)))  # Выводим результат
    count = sum(search in x for x in mw)  # Ищем наш шаблон
    print("Number of matching elements - ", count)

while True or OverflowError:
    main()
    again = input("Do you want to start the program again? [Y/N]: ")
    if again not in ["Y", "y"]:
        break
