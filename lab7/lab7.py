import math

gmw = []
fmw = []
ymw = []

try:
    data = list(map(float, input('Enter а,x,х maximum, number of calculation steps: ').split()))
    a = int(data[0])
    x = int(data[1])
    xmax = int(data[2])
    number = int(data[3])
    size = (xmax - x) / number  # Вычисляем размерность шагов

except ValueError:
    print("The input is incorrect")

for i in range(number):
    try:
        g = -(8*(7*a**2+34*a*x-5*x**2)/(27*a**2+33*a*x+10*x**2))
        gmw.append(g)
        f = -(1/(math.sin(72*a**2-5*a*x-12*x**2-(math.pi/2))))
        fmw.append(f)
        y = (math.log(42*a**2+53*a*x+15*x**2+1))
        ymw.append(y)
    except ZeroDivisionError:
        g = None
        gmw.append(g)
        f = None
        fmw.append(f)
    except ValueError:
        y = None
        ymw.append(y)
    x += size

file = open('arr.txt', 'w') # Работаем с файлом, если его нет создаем.
file.write(f"{gmw}\n{ymw}\n{fmw}")
file.close()

data = []

file = open('arr.txt', 'r')   # Открываем файл для чтения
[data.append(line.split()) for line in file]  # На каждой линии свое значение
file.close()

for x in range(len(data[0])):  # Выводим значения
    print(f"G: f(x) = {data[0][x]}")
for x in range(len(data[1])):
    print(f"F: f(x) = {data[1][x]}")
for x in range(len(data[2])):
    print(f"Y: f(x) = {data[2][x]}")
