import math

# Блок ввода данных программы
x = float(input("Введите x: "))
a = float(input("Введите a: "))

# Блок расчета G, F, Y
G = -(8*(7*a**2+34*a*x-5*x**2)/(27*a**2+33*a*x+10*x**2))
F = -(1/(math.sin(72*a**2-5*a*x-12*x**2-(math.pi/2))))
Y = (math.log(42*a**2+53*a*x+15*x**2+1))

# Блок вывода результатов
print("G = {0:.3f} ".format(G))
print("F = {0:.3f} " .format(F))
print("Y = {0:.3f} " .format(Y))

